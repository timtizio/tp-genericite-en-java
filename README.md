TP sur la généricité en JAVA

Ce TP est scindé en plusieurs partie. Chaque partie est matérialisé par une branche git


  * [énoncé du TP en PDF](https://gitlab.com/timtizio/tp-genericite-en-java/blob/master/TP-FSMIO-v2.pdf)
  * [Partie 1](https://gitlab.com/timtizio/tp-genericite-en-java/tree/partie1)
  * [Partie 2](https://gitlab.com/timtizio/tp-genericite-en-java/tree/partie2)

  
Partie 1
--------
    
Sortie de la question numéro 3 :    
    
+------ Nouvelle transition ------+    
| Etat origine : s1       |    
| Entrée : 'a'            |    
+---------------------------------+    
| Sortie : 0              |    
| Etat suivant : s1       |    
+---------------------------------+    
    
+------ Nouvelle transition ------+    
| Etat origine : s1       |    
| Entrée : 'b'            |    
+---------------------------------+    
| Sortie : 0              |    
| Etat suivant : s3       |    
+---------------------------------+    
    
+------ Nouvelle transition ------+    
| Etat origine : s3       |    
| Entrée : 'b'            |    
+---------------------------------+    
| Sortie : 1              |    
| Etat suivant : s3       |    
+---------------------------------+    
    
+------ Nouvelle transition ------+    
| Etat origine : s3       |    
| Entrée : 'a'            |    
+---------------------------------+    
| Sortie : 1              |    
| Etat suivant : s2       |    
+---------------------------------+    
    
+------ Nouvelle transition ------+    
| Etat origine : s2       |    
| Entrée : 'a'            |    
+---------------------------------+    
| Sortie : 0              |    
| Etat suivant : s1       |    
+---------------------------------+    
    
+------ Nouvelle transition ------+    
| Etat origine : s1       |    
| Entrée : 'b'            |    
+---------------------------------+    
| Sortie : 0              |    
| Etat suivant : s3       |    
+---------------------------------+    
    
+------ Nouvelle transition ------+    
| Etat origine : s3       |    
| Entrée : 'b'            |    
+---------------------------------+    
| Sortie : 1              |    
| Etat suivant : s3       |    
+---------------------------------+    
    
+------ Nouvelle transition ------+    
| Etat origine : s3       |    
| Entrée : 'a'            |    
+---------------------------------+    
| Sortie : 1              |    
| Etat suivant : s2       |    
+---------------------------------+    
    
+------ Nouvelle transition ------+    
| Etat origine : s2       |    
| Entrée : 'a'            |    
+---------------------------------+    
| Sortie : 0              |    
| Etat suivant : s1       |    
+---------------------------------+    
    
+------ Nouvelle transition ------+    
| Etat origine : s1       |    
| Entrée : 'a'            |    
+---------------------------------+    
| Sortie : 0              |    
| Etat suivant : s1       |    
+---------------------------------+    
    
Partie 2
--------
    
Sortie de la question 1 (Même entrées que la partie 1)    
    
+------ Nouvelle transition ------+    
| Etat origine : s1       |    
| Entrée : 'a'            |    
+---------------------------------+    
| Sortie : 0              |    
| Etat suivant : s1       |    
+---------------------------------+    
    
+------ Nouvelle transition ------+    
| Etat origine : s1       |    
| Entrée : 'b'            |    
+---------------------------------+    
| Sortie : 0              |    
| Etat suivant : s3       |    
+---------------------------------+    
    
+------ Nouvelle transition ------+    
| Etat origine : s3       |    
| Entrée : 'b'            |    
+---------------------------------+    
| Sortie : 1              |    
| Etat suivant : s3       |    
+---------------------------------+    
    
+------ Nouvelle transition ------+    
| Etat origine : s3       |    
| Entrée : 'a'            |    
+---------------------------------+    
| Sortie : 1              |    
| Etat suivant : s2       |    
+---------------------------------+    
    
+------ Nouvelle transition ------+    
| Etat origine : s2       |    
| Entrée : 'a'            |    
+---------------------------------+    
| Sortie : 0              |    
| Etat suivant : s1       |    
+---------------------------------+    
    
+------ Nouvelle transition ------+    
| Etat origine : s1       |    
| Entrée : 'b'            |    
+---------------------------------+    
| Sortie : 0              |    
| Etat suivant : s3       |    
+---------------------------------+    
    
+------ Nouvelle transition ------+    
| Etat origine : s3       |    
| Entrée : 'b'            |    
+---------------------------------+    
| Sortie : 1              |    
| Etat suivant : s3       |    
+---------------------------------+    
    
+------ Nouvelle transition ------+    
| Etat origine : s3       |    
| Entrée : 'a'            |    
+---------------------------------+    
| Sortie : 1              |    
| Etat suivant : s2       |    
+---------------------------------+    
    
+------ Nouvelle transition ------+    
| Etat origine : s2       |    
| Entrée : 'a'            |    
+---------------------------------+    
| Sortie : 0              |    
| Etat suivant : s1       |    
+---------------------------------+    
    
+------ Nouvelle transition ------+    
| Etat origine : s1       |    
| Entrée : 'a'            |    
+---------------------------------+    
| Sortie : 0              |    
| Etat suivant : s1       |    
+---------------------------------+    
    
    
Sortie de la question 9 (Avec pour transition le fichier src/main/test.txt, et les mêmes entrées que les questions précédentes)    
J'ai remplacé les sorties et les entrées par des strings permettant de comparer le résultat avec les questions précédentes.    
    
+------ Nouvelle transition ------+    
| Etat origine : s1       |    
| Entrée : '_a'           |    
+---------------------------------+    
| Sortie : zero           |    
| Etat suivant : s1       |    
+---------------------------------+    
    
+------ Nouvelle transition ------+    
| Etat origine : s1       |    
| Entrée : '_b'           |    
+---------------------------------+    
| Sortie : zero           |    
| Etat suivant : s3       |    
+---------------------------------+    
    
+------ Nouvelle transition ------+    
| Etat origine : s3       |    
| Entrée : '_b'           |    
+---------------------------------+    
| Sortie : un             |    
| Etat suivant : s3       |    
+---------------------------------+    
    
+------ Nouvelle transition ------+    
| Etat origine : s3       |    
| Entrée : '_a'           |    
+---------------------------------+    
| Sortie : un             |    
| Etat suivant : s2       |    
+---------------------------------+    
    
+------ Nouvelle transition ------+    
| Etat origine : s2       |    
| Entrée : '_a'           |    
+---------------------------------+    
| Sortie : zero           |    
| Etat suivant : s1       |    
+---------------------------------+    
    
+------ Nouvelle transition ------+    
| Etat origine : s1       |    
| Entrée : '_b'           |    
+---------------------------------+    
| Sortie : zero           |    
| Etat suivant : s3       |    
+---------------------------------+    
    
+------ Nouvelle transition ------+    
| Etat origine : s3       |    
| Entrée : '_b'           |    
+---------------------------------+    
| Sortie : un             |    
| Etat suivant : s3       |    
+---------------------------------+    
    
+------ Nouvelle transition ------+    
| Etat origine : s3       |    
| Entrée : '_a'           |    
+---------------------------------+    
| Sortie : un             |    
| Etat suivant : s2       |    
+---------------------------------+    
    
+------ Nouvelle transition ------+    
| Etat origine : s2       |    
| Entrée : '_a'           |    
+---------------------------------+    
| Sortie : zero           |    
| Etat suivant : s1       |    
+---------------------------------+    
    
+------ Nouvelle transition ------+    
| Etat origine : s1       |    
| Entrée : '_a'           |    
+---------------------------------+    
| Sortie : zero           |    
| Etat suivant : s1       |    
+---------------------------------+
